#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

typedef struct tagMsgHeader
{
    unsigned char major;
    unsigned char minor;
    unsigned short type;
    unsigned int length;
} MsgHeader;

typedef struct tagVideoDataHeader
{
    unsigned int eFrameType;
    unsigned int sequence;
} VideoDataHeader;

enum eMessageType
{
    kMsgTypeConnect = 1000,
    kMsgTypePublish = 1001,
    kMsgTypePublishAck = 1002,
    kMsgTypeVideoData = 1003,
    kMsgTypeVideoAck = 1004,
    kMsgTypeRequestKeyFrame = 1005,
    kMsgTypePicture = 1006,
    kMsgTypeOperate = 1007,
    kMsgTypeOperateAck = 1008,
    kMsgTypeMouseEvent = 1009,
    kMsgTypeKeyboardEvent = 1010,
    kMsgTypeStopStream = 1011,
    kMsgTypeStopStreamAck = 1012,
    kMsgTypeHeartbeat = 1013,
    kMsgTypeCursorShape = 1014,
    kMsgTypeAudioData = 1015,
    kMsgTypeStreamOnly = 1016,
    kMsgTypeStreamQuit = 1017
};

typedef enum
{
	videoFrameTypeInvalid, ///< encoder not ready or parameters are invalidate
	videoFrameTypeIDR,     ///< IDR frame in H.264
	videoFrameTypeI,       ///< I frame type
	videoFrameTypeP,       ///< P frame type
	videoFrameTypeSkip,    ///< skip the frame based encoder kernel
	videoFrameTypeIPMixed  ///< a frame where I and P slices are mixing, not supported yet
} EVideoFrameType;

#endif
