#ifndef __CLIENT_CONNECTION_H__
#define __CLIENT_CONNECTION_H__

#include "Session.h"

class ClientConnection
{
public:
    ClientConnection(string user_name, struct lws *wsi, Session *session);
    ~ClientConnection();

	void process_heartbeat();
	void process_publish(struct lws *wsi);
	void process_video_data(uint8_t* data, int len, struct lws *wsi);
	void process_picture_data(uint8_t* data, int len);
	void process_video_ack(uint8_t* data, int len);
	void process_request_key_frame(uint8_t* data, int len);
	void process_operate(struct lws *wsi);
	void process_input_event(uint8_t* data, int len);
	void process_stop_stream(struct lws *wsi);
	void process_cursor_shape(uint8_t* data, int len);
	void process_audio_data(uint8_t* data, int len);
	void process_stream_quit();

    struct lws* get_websocket_handle()
	{
		return m_wsi;
	}
	bool is_publisher()
	{
		return m_bPublisher;
	}
	bool is_operater()
	{
		return m_bOperater;
	}
	string get_user_name()
	{
		return m_strUserName;
	}
	void set_buffer_clear(bool bClear)
	{
	    m_bClear = bClear;
	}

private:
	void send_data(uint8_t* data, int len);

    struct lws *m_wsi;
	bool m_bClear;
	uint64_t m_uLastKeyRequestTime;
    Session *m_pSession;

    bool m_bPublisher;
    bool m_bOperater;
    string m_strUserName;
	uint8_t m_sSendBuf[LWS_PRE + COMMAND_BUFFER_SIZE];
};

#endif
