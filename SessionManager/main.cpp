#include <restbed>
#include "Json.h"
#include <list>
#include <thread>
#include <mutex>
#include <fstream>
#include <uuid/uuid.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>


using namespace restbed;

typedef struct tagNodeInfo
{
    std::string app_guid;
    std::string app_name;
    std::string host_name;
    int session_port;
    pid_t session_pid;
    int status;
} NodeInfo;


static std::list<NodeInfo> _node_list;
static std::mutex _list_lock;
static int _current_port = 20000;

static std::string get_app_guid()
{
    uuid_t uuid;
    char str[36];

    uuid_generate(uuid);
    uuid_unparse(uuid, str);
    return std::string(str);
}

static void sigchld_handler(int sig)
{
    if (sig == SIGCHLD) {
        int status = 0;
        int pid = waitpid(-1, &status, WNOHANG);
        printf("recv sigchld pid %d\n", pid);
        _list_lock.lock();
        for (std::list<NodeInfo>::iterator it = _node_list.begin(); it != _node_list.end(); it++) {
            if ((*it).session_pid == pid) {
                (*it).status = 1;
                (*it).session_pid = -1;
                break;
            }
        }
        _list_lock.unlock();
    }
}

static pid_t start_session(int session_port)
{
    pid_t pid = vfork();
    if (pid == 0) {
        execl("./session", "session", std::to_string(session_port).c_str(), NULL);
        exit(0);
    } else {
        printf("pid=%d, getpid=%d\n", pid, getpid());
        signal(SIGCHLD, sigchld_handler);
    }
    return pid;
}

void post_add_node_handler(const std::shared_ptr< Session > session)
{
    const auto request = session->get_request();

    int content_length = stoi(request->get_header("Content-Length"));
    session->fetch(content_length, [request](const std::shared_ptr< Session > session, const Bytes & body)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(std::string((const char*)body.data()), root))
        {
			session->close(NOT_ACCEPTABLE, "Data is not acceptable!");
            return;
        }

        std::string app_name;
        std::string host_name;
        std::string app_guid;
        int session_port = -1;
        if (root["app_name"].isString()) {
            app_name = root["app_name"].asString();
        }
        if (root["host_name"].isString()) {
            host_name = root["host_name"].asString();
        }
        printf("app_name %s, host_name %s\n", app_name.c_str(), host_name.c_str());
        bool found = false;
        pid_t pid = -1;
        _list_lock.lock();
        for (std::list<NodeInfo>::iterator it = _node_list.begin(); it != _node_list.end(); it++) {
            if ((*it).app_name == app_name && (*it).host_name == host_name) {
                found = true;
                app_guid = (*it).app_guid;
                session_port = (*it).session_port;
                if ((*it).status != 0) {
                    pid = start_session(session_port);
                    if (pid < 0) {
                        _list_lock.unlock();
                        session->close(INTERNAL_SERVER_ERROR, "Can not create session process!");
                        return;
                    }
                    (*it).session_pid = pid;
                    (*it).status = 0;
                }
                break;
            }
        }

        if (!found) {
            NodeInfo info;
            info.app_name = app_name;
            info.host_name = host_name;
            info.app_guid = get_app_guid();
            info.session_port = _current_port;
            _current_port += 2;
            pid = start_session(info.session_port);
            if (pid < 0) {
                _list_lock.unlock();
                session->close(INTERNAL_SERVER_ERROR, "Can not create session process!");
                return;
            }
            info.session_pid = pid;
            info.status = 0;
            _node_list.push_back(info);
            app_guid = info.app_guid;
            session_port = info.session_port;
        }
        _list_lock.unlock();

        root.clear();
        Json::FastWriter writer;
        root["app_guid"] = app_guid;
        root["session_port"] = session_port;
        std::string result = writer.write(root);
        session->set_header("Content-Length", std::to_string(result.length()));
        session->close(OK, result);
    } );
}

void post_del_node_handler(const std::shared_ptr< Session > session)
{
    const auto request = session->get_request();

    int content_length = stoi(request->get_header("Content-Length"));
    session->fetch(content_length, [request](const std::shared_ptr< Session > session, const Bytes & body)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(std::string((const char*)body.data()), root))
        {
            session->close(NOT_ACCEPTABLE, "Data is not acceptable!");
            return;
        }

        std::string app_guid;
        if (root["app_guid"].isString()) {
            app_guid = root["app_guid"].asString();
        }
        _list_lock.lock();
        for (std::list<NodeInfo>::iterator it = _node_list.begin(); it != _node_list.end(); it++) {
            if ((*it).app_guid == app_guid) {
	        pid_t pid = (*it).session_pid;
                if (pid > 0) {
                    kill(pid, SIGKILL);
                }
                _node_list.erase(it);
                break;
            }
        }
        _list_lock.unlock();
        session->close(OK, "");
    } );
}

void get_nodelist_handler(const std::shared_ptr< Session > session)
{
    Json::Value root;
    Json::FastWriter writer;
    int i = 0;
    _list_lock.lock();
    for (std::list<NodeInfo>::iterator it = _node_list.begin(); it != _node_list.end(); it++) {
        root[i]["app_guid"] = (*it).app_guid;
        root[i]["app_name"] = (*it).app_name;
        root[i]["session_port"] = (*it).session_port;
        root[i]["host_name"] = (*it).host_name;
        root[i]["status"] = (*it).status;
        i++;
    }
    _list_lock.unlock();
    std::string result = writer.write(root);
    session->set_header("Content-Length", std::to_string(result.length()));
    session->close(OK, result);
}

void post_file_handler(const std::shared_ptr< Session > session)
{
	const auto request = session->get_request();

    int content_length = stoi(request->get_header("Content-Length"));
	session->fetch(content_length, [request](const std::shared_ptr< Session > session, const Bytes & body)
	{
		const std::string filename = request->get_path_parameter("filename");
		if (access("/var/file", F_OK) != 0) {
            umask(0);
            mkdir("/var/file", S_IRWXU | S_IRWXG | S_IRWXO);
		}
		std::ofstream stream("/var/file/" + filename, std::ofstream::out | std::ofstream::binary);
		stream.write((const char*)body.data(), body.size());
		stream.close();
		session->close(OK, "post file succeed");
	});
}

void get_file_handler(const std::shared_ptr< Session > session)
{
    const auto request = session->get_request( );
    const std::string filename = request->get_path_parameter("filename");
    std::ifstream stream("/var/file/" + filename, std::ifstream::in | std::ifstream::binary);

    if (stream.is_open())
    {
		const std::string body = std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
        const std::multimap<std::string, std::string> headers
        {
        	{ "Content-Type", "application/octet-stream" },
            { "Content-Length", std::to_string(body.length()) }
        };
        session->close(OK, body, headers);
        stream.close();
    }
    else
    {
        session->close(NOT_FOUND);
    }
}

void get_filelist_handler(const std::shared_ptr< Session > session)
{

}

int main(int argc, char** argv)
{
	auto resource1 = std::make_shared< Resource >();
	resource1->set_path("/add-node");
	resource1->set_method_handler("POST", post_add_node_handler);

	auto resource2 = std::make_shared< Resource >();
	resource2->set_path("/del-node");
	resource2->set_method_handler("POST", post_del_node_handler);

	auto resource3 = std::make_shared< Resource >();
	resource3->set_path("/nodelist");
	resource3->set_method_handler("GET", get_nodelist_handler);

	auto resource4 = std::make_shared< Resource >();
	resource4->set_path("/file/{filename: .*}");
	resource4->set_method_handler("POST", post_file_handler);
	resource4->set_method_handler("GET", get_file_handler);

	auto resource5 = std::make_shared< Resource >();
	resource5->set_path("/filelist");
	resource5->set_method_handler("GET", get_filelist_handler);

	auto settings = std::make_shared< Settings >();
	if (argc > 1)
	{
		int port = atoi(argv[1]);
		if (port > 0) {
			settings->set_port(port);
		}
	}
	settings->set_default_header("Connection", "close");

	Service service;
	service.publish(resource1);
	service.publish(resource2);
	service.publish(resource3);
	service.start(settings);

    return EXIT_SUCCESS;
}
